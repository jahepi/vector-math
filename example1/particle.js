class Particle {
    
    constructor(x, y, speed, speedAngle, gravity, gravityAngle, friction, size) {
        this.position = new Vector(x, y);
        this.velocity = new Vector(0, 0);
        this.velocity.setLength(speed);
        this.velocity.setAngle(speedAngle);
        this.acceleration = new Vector(0, 0);
        this.gravity = new Vector(0, 0);
        this.gravity.setLength(gravity);
        this.gravity.setAngle(gravityAngle);
        this.friction = friction;
        this.color = '#ccc';
        this.size = size;
    }
    
    update() {
        this.position.addSelf(this.velocity);
        this.velocity.addSelf(this.acceleration);
        this.velocity.addSelf(this.gravity);
        this.velocity.multiplySelf(this.friction);
    }
    
    render(context) {
        context.fillStyle = this.color;
        context.fillRect(this.position.x, this.position.y, this.size, this.size);
    }
    
}


