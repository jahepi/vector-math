class Vector {
    
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    
    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }
    
    angle() {
        return Math.atan2(this.y, this.x);
    }
    
    normalize() {
        var length = this.length();
        return new Vector(this.x / length, this.y / length);
    }
    
    setAngle(radians) {
        var length = this.length();
        this.x = Math.cos(radians) * length;
        this.y = Math.sin(radians) * length;
    }
    
    setLength(length) {
        var radians = this.angle();
        this.x = Math.cos(radians) * length;
        this.y = Math.sin(radians) * length;
    }
    
    add(vector) {
        return new Vector(vector.x + this.x, vector.y + this.y);
    }
    
    substract(vector) {
        return new Vector(vector.x - this.x, vector.y - this.y);
    }
    
    multiply(scalar) {
        return new Vector(scalar * this.x, scalar * this.y);
    }
    
    divide(scalar) {
        return new Vector(this.x / scalar, this.y / scalar);
    }
    
    addSelf(vector) {
        this.x += vector.x;
        this.y += vector.y;
    }
    
    substractSelf(vector) {
        this.x -= vector.x;
        this.y -= vector.y;
    }
    
    multiplySelf(scalar) {
        this.x *= scalar;
        this.y *= scalar;
    }
    
    divideSelf(scalar) {
        this.x /= scalar;
        this.y /= scalar;
    }
    
    dot(vector) {
        var norm1 = this.normalize();
        var norm2 = vector.normalize();
        return norm1.x * norm2.x + norm1.y * norm2.y;
    }
    
    distance(vector) {
        var newVector = this.substract(vector);
        return newVector.length();
    }
}
